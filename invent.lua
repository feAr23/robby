invent = {}

function invent.last_cell_number()
  return robot.inventorySize()
end

function invent.search(name)
  local idx = 1
  local max = invent.last_cell_number()

  while idx <= max do
    local item = invent.slot_data(idx)

    if item == name then
      return idx
    end

    idx = idx + 1
  end

  print('Not found')
  return 0
end

function invent.slot_data(number)
  local item = component.inventory_controller.getStackInInternalSlot(number)

  if item == nil then
    return ''
  end

  return item['name']
end

function invent.select(name)
  local slot = invent.search(name)

  if slot == 0 then
    return 0
  end

  robot.select(slot)

  return slot
end
