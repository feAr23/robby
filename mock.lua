robot = {}
component = {}
component.inventory_controller = {}
component.generator = {}

function robot.forward()
  print('Двинулся вперёд')
  return true
end

function robot.turnRight()
  print('Повернулся направо')
  return true
end

function robot.turnLeft()
  print('Повернулся налево')
  return true
end

function robot.up()
  print('Полнялся')
  return true
end

function robot.down()
  print('Опустился')
  return true
end

function robot.placeDown()
  print('Поставил блок')
  return true
end

function component.inventory_controller.getStackInInternalSlot(number)
  result = {}
  result['name'] = 'test'
  return result
end

function component.generator.insert(count)
  return true
end
