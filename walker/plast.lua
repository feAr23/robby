walker.plast = {}

function walker.plast.run(forward, right)
  robot.forward()
  walker.plast.sey_start_position()

  local idx = 1
  while idx <= right do
    walker.plast.go_forward_line(forward)

    robot.turnRight()
    robot.forward()
    robot.turnLeft()

    idx = idx + 1
  end

  robot.turnLeft()

  local idx = 1
  while idx <= right do
    robot.forward()

    idx = idx + 1
  end

  robot.turnLeft()
  robot.forward()
  robot.turnRight()
  robot.turnRight()
end

function walker.plast.sey_start_position()
  print('Встал на изначальную позицию')
end

function walker.plast.go_forward_line(forward)
  local idx = 1
  while idx < forward do
    walker.plast.before_step()
    robot.forward()
    idx = idx + 1
  end
  walker.plast.before_step()

  robot.turnRight()
  robot.turnRight()

  idx = 1
  while idx < forward do
    robot.forward()
    idx = idx + 1
  end

  robot.turnRight()
  robot.turnRight()
end

function walker.plast.before_step()
end
