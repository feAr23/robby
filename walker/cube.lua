walker.cube = {}

function walker.cube.run(up, forward, right)
  local idx = 1
  while idx <= up do
    walker.plast.run(forward, right)

    robot.up()

    idx = idx + 1
  end

  local idx = 1
  while idx <= up do
    robot.down()

    idx = idx + 1
  end
end
