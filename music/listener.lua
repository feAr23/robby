component = require('component')
sides = require('sides')
event = require('event')
io = require('io')

local modem = component.modem

print('Какой порт слушать?')
local port = tonumber(io.read())
modem.open(port)

local function redstone_signal(side)
  local direction

  if side == 'left' then
    direction = sides.left
  end

  if side == 'right' then
    direction = sides.right
  end

  if side == 'back' then
    direction = sides.back
  end

  if side == 'forward' then
    direction = sides.forward
  end

  if direction == nil then
    return
  end

  component.redstone.setOutput(direction, 1)
  component.redstone.setOutput(direction, 0)
end

while true do
  local _, _, _, _, _, message = event.pull("modem_message")
  redstone_signal(tostring(message))
end
