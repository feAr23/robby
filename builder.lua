require('base')

builder = {}
builder.block_map = {}
builder.block_map['0'] = 'minecraft:diamond_block'
builder.data = {}
builder.data.print = '-----------------------------------------------------------------------------00---000---00----0000000000-000000000----000----00------000----00-------000---000---000---000----000-000----000---000----000-----000----000------000---000---000---000----00--000----00----000----000-----000----000------000---000---000-00000000-----000--------0000000000000--000000000000------000---000---000--0000000-----000---------0000000000000---000----000------000---000---000---000----00--000----00----000----000-----000----000------000000000000000---000----000-000----000---000----000-----000----000--------00000000000------000000000--000000000----00----00-------000000000----------------------------------------------------------------------------'
builder.data.arr = {}

for i = 1, #builder.data.print do
    builder.data.arr[i] = builder.data.print:sub(i, i)
end

local arr_idx = 1
function walker.plast.before_step()
  local key = builder.data.arr[arr_idx]
  arr_idx = arr_idx + 1

  local block_name = builder.block_map[key]

  if block_name == nil then
    return
  end

  invent.select(block_name)
  robot.placeDown()
end

walker.cube.run(10, 73, 1)
